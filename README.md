This a small app that shows every Marvel comic where Jack Kirby is credited as a creator.

The app employs personally implemented networking and coordinator solutions. Both of these are based on code put forward by other sources (specifically, Glenna Buford's UIKonf test for the networking solution and a combination of Soroush Khanlou and Ray Wenderlich's coordinator proposals.) See this code here:

[BLCoordinator](https://gitlab.com/bwlane/blcoordinator)
- removes relationship information between `ViewControllers` up a level to a `Coordinator` that governs these connections.
- pushes presentation and dismiss logic in a `ViewController` to a `ViewRouteDirector` object.

[BLNetworking](https://gitlab.com/bwlane/blnetworking)
- provides predictable networking operations and network requests using enumerations.

To use:

A user must log in! App credentials have been provided. They must be entered on the login screen. After pressing "login," the user will be authenticated against a Python Flask backend to obtain a secret Marvel API key. The app then ingests Marvel comic API data. Again, all the data is from comics that Jack Kirby worked on or inspired. A user can tap on any comic in the list for more details about that comic (if available) and a better view of the comic cover.

The app employs the coordinator pattern for separation of logic from view controllers. Some failures have been fudged (simple print messages). There are tests for the models used to ingest data, but the tests are by no means complete.

Watch the app in action! [Click here to view a gif of the app.](https://gitlab.com/bwlane/marvelapp/blob/master/brian_lane_marvel_app.gif?expanded=true&viewer=rich)
