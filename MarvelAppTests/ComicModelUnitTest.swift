//
//  ComicModelUnitTest.swift
//  MarvelAppTests
//
//  Created by Brian Lane on 1/1/20.
//  Copyright © 2020 Brian Lane. All rights reserved.
//

import XCTest
@testable import MarvelApp

class ComicModelUnitTest: XCTestCase {

    // System under test
    var sut: ComicModel!
    var isoDate: String!
    var id: Int!
    
    override func setUp() {
        super.setUp()
        sut = ComicModel()
    }
    
    func testComicModel_hasNullValues_onBaseInit() {
        XCTAssertEqual(nil, sut.attributionText)
        XCTAssertEqual(nil, sut.title)
        XCTAssertEqual(nil, sut.id)
        XCTAssertEqual(nil, sut.releaseDate)
        XCTAssertEqual(nil, sut.issueNumber)
        XCTAssertEqual(nil, sut.imageExtension)
        XCTAssertEqual(nil, sut.imageURL)
    }
    
    func testComicModel_descriptionIsEmptyString_onBaseInit() {
        XCTAssertEqual("", sut.description)
    }
    
    func testComicModel_producesPrettyReleaseDate_whenStandardReleaseDateIsSet() {
        // given
        isoDate = "2019-12-18T00:00:00-0500"
        sut.releaseDate = isoDate
        
        //when
        let prettyDate = sut.releaseDatePretty
        let expectedPrettyDate = "dec 18, 2019"
        
        // then
        XCTAssertEqual(prettyDate!.lowercased(), expectedPrettyDate)
    }
    
    func testComicModel_producesPrettyID_whenIDIsSet() {
        // given
        id = 1000
        sut.id = 1000
        
        //when
        let stringID = sut.idAsString
        
        //then
        XCTAssertEqual(stringID!, "1000")
    }

    override func tearDown() {
        sut = nil
        isoDate = nil
        super.tearDown()
    }

}
