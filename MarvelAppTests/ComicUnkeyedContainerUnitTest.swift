//
//  UnkeyedComicModelUnitTest.swift
//  MarvelAppTests
//
//  Created by Brian Lane on 1/1/20.
//  Copyright © 2020 Brian Lane. All rights reserved.
//

import XCTest
@testable import MarvelApp

class ComicUnkeyedContainerUnitTest: XCTestCase {
    
    // System under test
    var sut_bare: ComicUnkeyedContainer!
    var sut_fromJson: ComicUnkeyedContainer!
    var json: String!
    
    override func setUp() {
        super.setUp()
        sut_bare = ComicUnkeyedContainer()
    }
    
    func testComicUnkeyedContainer_hasAllOptionalValues_whenInitBare() {
        XCTAssertEqual(nil, sut_bare.available)
        XCTAssertEqual(nil, sut_bare.offset)
        XCTAssertEqual(nil, sut_bare.total)
    }
    
    func testComicUnkeyedContainer_hasCorrectValues_whenInitFromJson() {
        //given (should load from test file...)
        json = """
        {
        "code": 200,
        "status": "Ok",
        "copyright": "© 2019 MARVEL",
        "attributionText": "Data provided by Marvel. © 2019 MARVEL",
        "data": {
        "offset": 20,
        "limit": 20,
        "total": 912,
        "count": 20,
        "results": [{
        "id": 75499,
        "digitalId": 0,
        "title": "Captain America: Evolutions Of A Living Legend (Trade Paperback)",
        "issueNumber": 0,
        "variantDescription": "",
        "description": " Collects Captain America (1968) #180, #337, #438 And #451; Captain America (1996) #3; Secret Avengers (2010) #1; Captain America (2012) #1; Captain America: Steve Rogers #1; Captain America (2017) #695 and material from Captain America Comics #1-2. The many costumes of Captain America! The star-spangled costume of Captain America has been a timeless symbol of hope and freedom since his days fighting Nazism overseas and McCarthyism at home. This historical retrospective of Steve Rogers’ various uniforms and super hero mantles is a showcase of America’s ever-evolving sociopolitical landscape. From his early days fighting in overt patriotic garb as Captain America during World War II through his adoption of the predominately black uniform and title of the Captain at a time when he became a symbol of resisting absolute government control, Rogers has always worn his allegiance openly. Time and again, Steve has returned to Captain America’s red-white-and-blue iconography, proving that the symbolic clothing of the Sentinel of Liberty stands for a higher ideal than any one person or government can achieve. ",
        "modified": "2019-05-30T16:10:43-0400",
        "isbn": "978-1-302-91848-4",
        "upc": "",
        "diamondCode": "MAR191008",
        "ean": "9781302 918484 52999",
        "issn": "",
        "format": "Trade Paperback",
        "pageCount": 248,
        "textObjects": [{
        "type": "issue_solicit_text",
        "language": "en-us",
        "text": " Collects Captain America (1968) #180, #337, #438 And #451; Captain America (1996) #3; Secret Avengers (2010) #1; Captain America (2012) #1; Captain America: Steve Rogers #1; Captain America (2017) #695 and material from Captain America Comics #1-2. The many costumes of Captain America! The star-spangled costume of Captain America has been a timeless symbol of hope and freedom since his days fighting Nazism overseas and McCarthyism at home. This historical retrospective of Steve Rogers’ various uniforms and super hero mantles is a showcase of America’s ever-evolving sociopolitical landscape. From his early days fighting in overt patriotic garb as Captain America during World War II through his adoption of the predominately black uniform and title of the Captain at a time when he became a symbol of resisting absolute government control, Rogers has always worn his allegiance openly. Time and again, Steve has returned to Captain America’s red-white-and-blue iconography, proving that the symbolic clothing of the Sentinel of Liberty stands for a higher ideal than any one person or government can achieve. "
        }],
        "resourceURI": "http://gateway.marvel.com/v1/public/comics/75499",
        "urls": [{
        "type": "detail",
        "url": "http://marvel.com/comics/collection/75499/captain_america_evolutions_of_a_living_legend_trade_paperback?utm_campaign=apiRef&utm_source=09350948f6cd8dca1e08e6e9448fad06"
        },
        {
        "type": "purchase",
        "url": "http://comicstore.marvel.com/CAPTAIN-AMERICA-EVOLUTIONS-OF-A-LIVING-LEGEND-TPB-0/digital-comic/51705?utm_campaign=apiRef&utm_source=09350948f6cd8dca1e08e6e9448fad06"
        }
        ],
        "series": {
        "resourceURI": "http://gateway.marvel.com/v1/public/series/27145",
        "name": "Captain America: Evolutions Of A Living Legend (2019)"
        },
        "variants": [],
        "collections": [],
        "collectedIssues": [],
        "dates": [{
        "type": "onsaleDate",
        "date": "2019-06-05T00:00:00-0400"
        },
        {
        "type": "focDate",
        "date": "2019-04-15T00:00:00-0400"
        }
        ],
        "prices": [{
        "type": "printPrice",
        "price": 29.99
        }],
        "thumbnail": {
        "path": "http://i.annihil.us/u/prod/marvel/i/mg/d/30/5ceea8b95863f",
        "extension": "jpg"
        },
        "images": [{
        "path": "http://i.annihil.us/u/prod/marvel/i/mg/d/30/5ceea8b95863f",
        "extension": "jpg"
        }]
        }]
        }
        }
        """
        
        //when
        sut_fromJson = try! JSONDecoder().decode(ComicUnkeyedContainer.self, from: json.data(using: .utf8)!)
        let comic = sut_fromJson.comics?.first!
        
        //then
        XCTAssertEqual(1, sut_fromJson.comics!.count)
        XCTAssertEqual(912, sut_fromJson.total!)
        XCTAssertEqual(20, sut_fromJson.offset!)
        XCTAssertEqual("Captain America: Evolutions Of A Living Legend (Trade Paperback)", comic?.title!)
        XCTAssertEqual(75499, comic?.id!)
        XCTAssertEqual(0, comic?.issueNumber!)
    XCTAssertEqual("http://i.annihil.us/u/prod/marvel/i/mg/d/30/5ceea8b95863f", comic?.imageURL!)
        XCTAssertEqual("jpg", comic?.imageExtension!)
        XCTAssertEqual("2019-06-05T00:00:00-0400", comic?.releaseDate!)
        
    }
    
    override func tearDown() {
        json = nil
        sut_bare = nil
        sut_fromJson = nil
        super.tearDown()
    }

    
}
