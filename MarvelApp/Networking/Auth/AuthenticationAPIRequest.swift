//
//  AuthenticationAPIRequest.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/30/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import Foundation
import BLNetworking

enum AuthenticationAPIRequest {
    static func authenticateWithBackEnd(username: String, password: String, completion: @escaping(Result<KeyResponse?, Error>) -> Void) {
        let op: DecodableNetworkOperation<KeyResponse> = DecodableNetworkOperation<KeyResponse>(with: AuthenticationAPIRouter.login(username: username, password: password)) { result in
            switch result {
            case .success(let response):
                if let resp = response {
                    completion(.success(resp))
                } else {
                    completion(.success(nil))
                }
            case .failure(let error):
                print("\(error)")
                completion(.failure(error))
            }
        }
        NetworkService.shared.add(op)
    }
}
