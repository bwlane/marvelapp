//
//  AuthenticationAPIRouter.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/30/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import Foundation
import BLNetworking

enum AuthenticationAPIRouter: APIRouter {
    case login(username: String, password: String)
    
    var urlRequest: URLRequest {
        let urlString = "\(host)\(route)"
        let url = URL(string: urlString)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod.rawValue
        if let body = body {
            urlRequest.httpBody = body
        }
        urlRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        return urlRequest
    }
    
    private var host: String {
        switch self {
        case .login:
            return "https://brianwlane.pythonanywhere.com/api/"
        }
    }
    
    private var route: String {
        switch self {
        case .login:
            return "get_key"
        }
    }
    
    private var httpMethod: HTTPMethod {
        switch self {
        case .login:
            return .post
        }
    }
        
    private var body: Data? {
        switch self {
        case .login(let username, let password):
            let json = ["user":username, "password": password]
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                return jsonData
            } catch let error {
                print("😱 Error -> \(error)")
            }
            return nil
        }
    }
}
