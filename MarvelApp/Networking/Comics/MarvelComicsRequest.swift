//
//  MarvelComicsRequest.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/29/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit
import BLNetworking

enum UserFacingError: Error {
    case networkError(NetworkError)
    case parsingError
}

enum MarvelComicsRequest {
    
    static func paginatedListOfComicsAsData(key: String, offset: Int, completion: @escaping (Result<ComicUnkeyedContainer, Error>) -> Void) {
        let op: DecodableNetworkOperation<ComicUnkeyedContainer> = DecodableNetworkOperation<ComicUnkeyedContainer>(with: MarvelComicsAPIRouter.listOfComics(key: key, offset: offset)) { result in
            switch result {
            case .success(let comicsList):
                if let comicsList = comicsList {
                    completion(.success(comicsList))
                } else {
                    completion(.failure(UserFacingError.parsingError))
                }
            case .failure(let error):
                completion(.failure(error))
                print("😰 Failed here: \(error)")
            }
        }
        
        NetworkService.shared.add(op)
    }
    
    static func getCoverImagePortrait(urlString: String, type imageType: ImageType, completion: @escaping (Result<UIImage?, Error>) -> Void) {
        let op = ImageNetworkOperation<UIImage>(with: MarvelComicsAPIRouter.comicImage(type: imageType, urlString: urlString)) { result in
            switch result {
            case .success(let image):
                if let image = image as? UIImage {
                    completion(.success(image))
                } else {
                    completion(.success(nil))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
        NetworkService.shared.add(op)
    }
  
}

