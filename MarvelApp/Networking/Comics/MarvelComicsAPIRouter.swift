//
//  MarvelComicsAPIRouter.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/29/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//
import UIKit
import BLNetworking

enum MarvelComicsAPIRouter: APIRouter {
    case listOfComics(key: String, offset: Int) // associated value is offset
    case comicImage(type: ImageType, urlString: String) // associated value is comic image id
    
    var urlRequest: URLRequest {
        switch self {
        case .listOfComics(let key, _):
            let ts = String.medievalTimeStamp
            let hash = "\(ts)\(key)\(publicApiKey)".utf8.md5
            let urlString = "\(host)\(route)?apikey=\(publicApiKey)&hash=\(hash)&ts=\(ts)\(self.offset)"
            let url = URL(string: urlString)!
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = httpMethod.rawValue
            return urlRequest
        case .comicImage:
            let urlString = "\(host)\(route)"
            let url = URL(string: urlString)!
            let urlRequest = URLRequest(url: url)
            return urlRequest
        }
    }
    
    private var host: String {
        switch self {
        case .listOfComics:
            return "https://gateway.marvel.com/v1/public/"
        case .comicImage( _, let urlString):
            return urlString
        }
    }
    
    private var offset: String {
        switch self {
        case .listOfComics(_, let offset):
            return "&offset=\(offset)"
        default:
            return ""
        }
    }
    
    private var route: String {
        switch self {
        case .listOfComics:
            return "creators/196/comics"
        case .comicImage(let type, _):
            switch type {
            case .detail:
                return "/portrait_xlarge.jpg"
            case .fullsize:
                return ".jpg"
            }
        }
    }
    
    private var httpMethod: HTTPMethod {
        switch self {
        case .comicImage, .listOfComics:
            return .get
        }
    }
}


