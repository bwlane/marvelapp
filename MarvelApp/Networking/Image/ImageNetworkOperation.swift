//
//  ImageNetworkOperation.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/30/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit
import BLNetworking

class ImageNetworkOperation<T: UIImage>: NetworkOperation<Any> {
    override func performRequest() {
        let task = URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            guard let self = self else { return }
            defer { self.operationState = .finished }
            if let error: Error = error {
                self.completion?(Result.failure(error))
            } else {
                guard let data = data else {
                    self.completion?(Result.success(nil))
                    return
                }
                let image = UIImage(data: data)
                self.desiredResult = image
                self.completion?(Result.success(image))
            }
        }
        task.resume()
    }
}

