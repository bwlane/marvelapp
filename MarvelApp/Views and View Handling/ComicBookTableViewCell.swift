//
//  ComicBookTableViewCell.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/30/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit

class ComicBookTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        activityIndicator.hidesWhenStopped = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureWith(_ comic: ComicModel?, and image: UIImage?) {
        switch (comic, image) {
        case (.some(let comic), .some(let image)):
            self.activityIndicator.stopAnimating()
            self.titleLabel.isHidden = false
            self.releaseDateLabel.isHidden = false
            self.coverImageView.isHidden = false
            if let title = comic.title {
                self.titleLabel.text = title
            }
            if let releaseDate = comic.releaseDate {
                let dateFormatter = ISO8601DateFormatter()
                let date = dateFormatter.date(from: releaseDate)!
                let calendar = Calendar.current
                let components = calendar.dateComponents([.year, .month, .day], from: date)
                let finalDate = calendar.date(from: components)
                let df = DateFormatter()
                df.dateStyle = .medium
                let dateString = df.string(from: finalDate ?? Date())
                self.releaseDateLabel.text = dateString
            }
            self.coverImageView.image = image
        case (.some(let comic), .none):
            self.activityIndicator.stopAnimating()
            self.titleLabel.isHidden = false
            self.releaseDateLabel.isHidden = false
            self.coverImageView.isHidden = false
            if let title = comic.title {
                self.titleLabel.text = title
            }
            if let releaseDate = comic.releaseDatePretty {
                self.releaseDateLabel.text = releaseDate
            }
            coverImageView.image = #imageLiteral(resourceName: "no_cover_available")
        default:
            if self.activityIndicator.isHidden {
                self.activityIndicator.startAnimating()
                self.titleLabel.isHidden = true
                self.releaseDateLabel.isHidden = true
                self.coverImageView.isHidden = true
            }
        }
    }
    
    func configureWith(image: UIImage) {
        self.coverImageView.image = image
    }
    
}
