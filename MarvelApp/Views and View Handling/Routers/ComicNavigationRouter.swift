//
//  ComicNavigationRouter.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/27/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit
import BLCoordinator

public class ComicNavigationRouter: NSObject {
    
    // MARK: - Instance Properties
    private unowned let navigationController: UINavigationController
    private var onDismissForViewController: [UIViewController: (() -> Void)] = [:]
    
    // MARK: - Object Lifecycle
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        super.init()
        self.navigationController.delegate = self
    }
}

// MARK: - NavigationControllerDelegate
extension ComicNavigationRouter: UINavigationControllerDelegate {
    public func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let dismissedVC = navigationController.transitionCoordinator?.viewController(forKey: .from), !navigationController.viewControllers.contains(dismissedVC) else {
            return
        }
        performOnDismissed(for: dismissedVC)
    }
}

// MARK: - Router
extension ComicNavigationRouter: ViewRouteDirector {
    public func present(_ viewController: UIViewController, animated: Bool, onDismissed: (() -> Void)?) {
        onDismissForViewController[viewController] = onDismissed
        navigationController.pushViewController(viewController, animated: animated)
    }
    
    public func dismiss(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }
    
    private func performOnDismissed(for viewController: UIViewController) {
        guard let onDismiss = onDismissForViewController[viewController] else {
            return
        }
        onDismiss()
        onDismissForViewController[viewController] = nil
    }
}
