//
//  AppDelegateRouter.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/27/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import  UIKit
import BLCoordinator

public class AppDelegateRouter: ViewRouteDirector {
    //MARK: - Instance Properties
    public let window: UIWindow
    public let navigationController = UINavigationController()
    
    //MARK: - Object Lifecycle
    public init(window: UIWindow) {
        self.window = window
        navigationController.navigationBar.isHidden = true
    }

    //MARK: - Instance Methods
    public func present(_ viewController: UIViewController, animated: Bool, onDismissed: (() -> Void)?) {
        navigationController.setViewControllers([viewController], animated: false)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    // TODO: (brianlane) Unimplemented as not needed
    public func dismiss(animated: Bool) {}
    
}
