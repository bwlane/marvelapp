//
//  ComicListViewController.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/28/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit
import BLCoordinator

public enum ImageType {
    case detail, fullsize
}

public protocol ComicListViewControllerDelegate: AnyObject {
    var detailImagesCache: NSCache<NSString, UIImage> { get set }
    func comicListViewControllerDidSelect(comic: ComicModel)
    func save(image: UIImage, of type: ImageType, for comic: ComicModel)
    var secretAPIKey: String { get }
}

public class ComicListViewController: UIViewController {
    
    //MARK: - Instance Properties
    public weak var delegate: ComicListViewControllerDelegate?
    public var comicsList = [ComicModel?]()
    @IBOutlet weak var tableView: UITableView!
    private let comicBookCell = "comicBookCell"
    private var offset = 0
    private var total = 0
    private var isFetching = false
    
    public override func viewDidLoad() {
        configureTableView()
        preFetchJackKirbyData(key: delegate?.secretAPIKey)
    }
}
//MARK: - Storyboard Instantiable
extension ComicListViewController: StoryboardInstantiable {
    public class func instantiate(delegate: ComicListViewControllerDelegate) -> ComicListViewController {
        let vc = instanceFromStoryboard()
        //TODO: set list of comics?
        vc.delegate = delegate
        return vc
    }
}

//MARK: - UITableViewDelegate
extension ComicListViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let comic = comicsList[indexPath.row] else { return }
        delegate?.comicListViewControllerDidSelect(comic: comic)
    }
}
//MARK: - UITableViewDataSource
extension ComicListViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return total
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: comicBookCell, for: indexPath) as! ComicBookTableViewCell
        if !isLoadingCell(for: indexPath) {
            if let comic = comicsList[indexPath.row] {
                if let id = comic.idAsString, let image = delegate?.detailImagesCache.object(forKey: id as NSString) {
                    cell.configureWith(comic, and: image)
                } else {
                    cell.configureWith(comic, and: .none)
                    fetchImage(for: comic, of: .detail, at: indexPath)
                }
            }
        }
        return cell
    }
}

//MARK: - PrefetchDataSource
extension ComicListViewController: UITableViewDataSourcePrefetching {
    public func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if indexPaths.contains(where: isLoadingCell) {
            preFetchJackKirbyData(key: delegate?.secretAPIKey)
        }
    }
}

private extension ComicListViewController {
    func isLoadingCell(for indexPath: IndexPath) -> Bool {
        return indexPath.row >= comicsList.count
    }
    
    func visibleIndexPathsToReload(intersecting indexPaths: [IndexPath]) -> [IndexPath] {
        let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows ?? []
        let indexPathsIntersection = Set(indexPathsForVisibleRows).intersection(indexPaths)
        return Array(indexPathsIntersection)
    }
}

//MARK: - Instance Properties
extension ComicListViewController {
    func configureTableView() {
        tableView.isHidden = true
        tableView.dataSource = self
        tableView.delegate = self
        tableView.prefetchDataSource = self
        tableView.register(UINib(nibName: "ComicBookTableViewCell", bundle: nil), forCellReuseIdentifier: comicBookCell)
    }
    
    func preFetchJackKirbyData(key: String?) {
        guard let key = key else { return }
        if isFetching || (offset != 0 && offset >= total) { return }
        isFetching = true
        MarvelComicsRequest.paginatedListOfComicsAsData(key: key, offset: offset) { result in
            switch result {
            case .success(let comics):
                if let total = comics.total, let comics = comics.comics {
                    self.isFetching = false
                    self.total = total
                    self.comicsList.append(contentsOf: comics)
                    self.offset += comics.count
                    DispatchQueue.main.async {
                        self.onFetchCompleted(with: .none)
                    }
                }
            case .failure(let error):
                self.isFetching = false
                print("🤢 Failure here! \(error)")
            }
        }
    }
    
    func calculateIndexPathsToReload() -> [IndexPath] {
        let startIndex = comicsList.count - 20
        let endIndex = startIndex + 20
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0)}
    }
    
    func onFetchCompleted(with newIndexPathsToReload: [IndexPath]?) {
        guard let newIndexPathsToReload = newIndexPathsToReload else {
            tableView.isHidden = false
            tableView.reloadData()
            return
        }
        let indexPathsToReload = visibleIndexPathsToReload(intersecting: newIndexPathsToReload)
        tableView.reloadRows(at: indexPathsToReload, with: .automatic)
    }
    
    func fetchImage(for comic: ComicModel, of type: ImageType, at indexPath: IndexPath) {
        guard let imageURL = comic.imageURL else { return }
        MarvelComicsRequest.getCoverImagePortrait(urlString: imageURL, type: type){ result in
            switch result {
            case .success(let image):
                if let image = image {
                    self.delegate?.save(image: image, of: type, for: comic)
                    DispatchQueue.main.async {
                        if let visiblePaths = self.tableView.indexPathsForVisibleRows, visiblePaths.contains(indexPath), let cell = self.tableView.cellForRow(at: indexPath) as? ComicBookTableViewCell {
                            cell.configureWith(image: image)
                            self.tableView.reloadRows(at: [indexPath], with: .automatic)
                        }
                    }
                }
            case .failure(let error):
                print("👎🏼 Couldn't get the image: \(error)")
            }
            
        }
    }
}
