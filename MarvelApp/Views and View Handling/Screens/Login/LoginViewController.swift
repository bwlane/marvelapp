//
//  LoginViewController.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/26/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit
import BLCoordinator

// MARK: - LoginViewControllerDelegate
public protocol LoginViewControllerDelegate: AnyObject {
    func loginViewControllerDidLogin(
        username: String,
        password: String,
        viewController: LoginViewController
    )
}

// MARK: - LoginViewController
public class LoginViewController: UIViewController {
    // MARK: - Instance Properties
    public weak var delegate: LoginViewControllerDelegate?
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var incorrectCredentialsLabel: UILabel!
    var spinner = UIActivityIndicatorView()
    
    public override func viewDidLoad() {
        DispatchQueue.main.async {
            self.setUpViews()
        }
    }
    
    // MARK: - Actions
    @IBAction internal func didPressLogin(_ sender: AnyObject) {
        view.isUserInteractionEnabled = false
        if let usernameText = usernameTextField.text, let passwordText = passwordTextField.text, !usernameText.isEmpty, !passwordText.isEmpty {
            delegate?.loginViewControllerDidLogin(username: usernameText, password: passwordText, viewController: self)
        } else {
            DispatchQueue.main.async {
                self.loginDidFail()
            }
        }
    }
    
    public func loginDidFail() {
        UIView.animate(withDuration: 0.4, animations: {
                self.incorrectCredentialsLabel.isHidden = false
                self.incorrectCredentialsLabel.alpha = 1
        }, completion: { (finished: Bool) in
            self.view.isUserInteractionEnabled = true
        })
    }
    
    private func setUpViews() {
        incorrectCredentialsLabel.isHidden = true
        spinner.hidesWhenStopped = true
        spinner.style = .large
        spinner.color = .red
        self.view.addSubview(self.spinner)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        spinner.alpha = 0
        if isIpad() {
            // Remove an unfriendly constraint error related to iPad keyboard
            let _ = [usernameTextField, passwordTextField].map { field in
                guard let field = field else { return }
                field.inputAssistantItem.leadingBarButtonGroups = []
                field.inputAssistantItem.trailingBarButtonGroups = []
            }
        }
        DispatchQueue.main.async {
            self.incorrectCredentialsLabel.alpha = 0
        }
    }
    
    public func isLoading() {
        spinner.startAnimating()
        spinner.alpha = 1
    }
    
    public func stopLoading() {
        spinner.stopAnimating()
        spinner.alpha = 0
    }
}

// MARK: - StoryboardInstantiable Conformance
extension LoginViewController: StoryboardInstantiable {
    public class func instantiate(delegate: LoginViewControllerDelegate) -> LoginViewController {
        let vc = instanceFromStoryboard()
        vc.delegate = delegate
        return vc
    }
}
