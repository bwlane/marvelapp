//
//  ComicDetailViewController.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/28/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit
import BLCoordinator

//MARK: - ComicDetailViewControllerDelegate
public protocol ComicDetailViewControllerDelegate: AnyObject {
    func comicDetailViewControllerPressedBack(_ controller: ComicDetailViewController)
    func save(image: UIImage, of type: ImageType, for comic: ComicModel)
    var fullSizeImagesCache: NSCache<NSString, UIImage> { get set }
    var secretAPIKey: String { get }
}

public class ComicDetailViewController: UIViewController {
    // MARK: - Instance Properties
    public weak var delegate: ComicDetailViewControllerDelegate?
    public var comic: ComicModel?
    
    // MARK: - IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var coverDetailView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var issueNumber: UILabel!
    
    public override func viewDidLoad() {
        setUpViews()
    }
    
    public override func viewDidLayoutSubviews() {
        scrollView.delegate = self
        scrollView.contentSize = CGSize(width: contentView.frame.size.width, height: contentView.frame.size.height + stackView.frame.size.height)
    }
    
    
    @IBAction func didPressBackButton(_ sender: Any) {
        self.delegate?.comicDetailViewControllerPressedBack(self)
    }
    
    private func fetchImage(for comic: ComicModel, of type: ImageType) {
        guard let imageURL = comic.imageURL else { return }
        MarvelComicsRequest.getCoverImagePortrait(urlString: imageURL, type: type){ result in
            switch result {
            case .success(let image):
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if let image = image {
                        self.delegate?.save(image: image, of: type, for: comic)
                        
                        if let imageView = self.coverDetailView {
                            imageView.image = image
                        }
                    }
                }
            case .failure(let error):
                print("👎🏼 Couldn't get the image: \(error)")
                
            }
            
        }
    }
    
    func setUpViews() {
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        backButton.layer.cornerRadius = 15
        backButton.clipsToBounds = true
        if let comic = comic {
            if let id = comic.idAsString, let image = delegate?.fullSizeImagesCache.object(forKey: id as NSString), let coverDetailView = coverDetailView {
                coverDetailView.image = image
            } else {
                fetchImage(for: comic, of: .fullsize)
            }
            titleLabel.text = comic.title ?? "No Title Available"
            releaseDate.text = comic.releaseDatePretty ?? "No Release Date Available"
            descriptionLabel.text = comic.description ?? "Description Unavailable!"
            let ishNumber = (comic.issueNumber != nil && comic.issueNumber != 0) ? "#\(comic.issueNumber!)" : "N/A"
            issueNumber.text = ishNumber
        }
    }
}

extension ComicDetailViewController: StoryboardInstantiable {
    public class func instantiate(delegate: ComicDetailViewControllerDelegate) -> ComicDetailViewController {
        let vc = instanceFromStoryboard()
        vc.delegate = delegate
        return vc
    }
}

extension ComicDetailViewController: UIScrollViewDelegate {}
