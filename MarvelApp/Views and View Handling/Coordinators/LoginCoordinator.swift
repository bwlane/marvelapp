//
//  LoginCoordinator.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/27/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit
import BLCoordinator

public class LoginCoordinator: Coordinator {
    
    //MARK: - Instance Properties
    public var children: [Coordinator] = []
    public let router: ViewRouteDirector
    
    //MARK: - Object Lifecycle
    public init(router: ViewRouteDirector) {
        self.router = router
    }
    
    //MARK: Instance Methods
    public func present(animated: Bool, onDismissed: (() -> Void)?) {
        let vc = LoginViewController.instantiate(delegate: self)
        router.present(vc, animated: animated, onDismissed: onDismissed)
    }
}

// MARK: - LoginViewControllerDelegate
extension LoginCoordinator: LoginViewControllerDelegate {
    public func loginViewControllerDidLogin(username: String, password: String, viewController: LoginViewController) {
        authenticate(username, password: password, from: viewController)
    }
    
    private func authenticate(_ username: String, password: String, from viewController: UIViewController) {
        if let loginVC = viewController as? LoginViewController {
            DispatchQueue.main.async {
                loginVC.isLoading()
            }
        }
        AuthenticationAPIRequest.authenticateWithBackEnd(username: username, password: password) { [weak self] result in
            guard let self = self else { return }
            if let loginVC = viewController as? LoginViewController {
                DispatchQueue.main.async {
                    loginVC.stopLoading()
                }
            }
        
        switch result {
        case .success(let data):
            if let data = data, let key = data.key, let router = self.router as? AppDelegateRouter {
                DispatchQueue.main.async {
                    
                    let nc = router.navigationController
                    let childRouter = ComicNavigationRouter(navigationController: nc)
                    let child = ComicCoordinator(router: childRouter, secretAPIKey: key)
                    self.presentChild(child, animated: true)
                }
            } else {
                if let vc = viewController as? LoginViewController {
                    DispatchQueue.main.async {
                        vc.loginDidFail()
                    }
                }
            }
        case .failure(let error):
            if let vc = viewController as? LoginViewController {
                DispatchQueue.main.async {
                    vc.loginDidFail()
                }
            }
            print("🤬 Error!: \(error)")
        }
    }
}
}
