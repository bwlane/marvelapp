//
//  ComicCoordinator.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/27/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit
import BLCoordinator

public class ComicCoordinator: Coordinator {
    // MARK: - Instance Properties
    public var children: [Coordinator] = []
    public let router: ViewRouteDirector
    public var detailImagesCache: NSCache<NSString, UIImage>
    public var fullSizeImagesCache: NSCache<NSString, UIImage>
    public let secretAPIKey: String
    
    // MARK: - Object Lifecycle
    public init(router: ViewRouteDirector, secretAPIKey: String) {
        self.router = router
        self.secretAPIKey = secretAPIKey
        self.detailImagesCache = NSCache<NSString, UIImage>()
        self.fullSizeImagesCache = NSCache<NSString, UIImage>()
    }
    // MARK: - Instance Methods
    public func present(animated: Bool, onDismissed: (() -> Void)?) {
        let vc = ComicListViewController.instantiate(delegate: self)
        router.present(vc, animated: true, onDismissed: onDismissed)
    }
    
    public func dismiss(animated: Bool) {
        router.dismiss(animated: animated)
    }

}
    // MARK: - ComicListViewControllerDelegate
extension ComicCoordinator: ComicListViewControllerDelegate {
    public func comicListViewControllerDidSelect(comic: ComicModel) {
        let vc = ComicDetailViewController.instantiate(delegate: self)
        vc.comic = comic
        router.present(vc, animated: true)
    }
    
    public func save(image: UIImage, of type: ImageType, for comic: ComicModel) {
        let id = comic.idAsString! as NSString
        switch type {
        case .detail:
            detailImagesCache.setObject(image, forKey: id)
        case .fullsize:
            fullSizeImagesCache.setObject(image, forKey: id)
        }
    }
}
// MARK: - ComicDetailViewControllerDelegate
extension ComicCoordinator: ComicDetailViewControllerDelegate {
    public func comicDetailViewControllerPressedBack(_ controller: ComicDetailViewController) {
        router.dismiss(animated: true)
    }
}
