//
//  AppDelegate.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/26/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit
import BLCoordinator

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: - Instance Properties
    public lazy var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
    public lazy var coordinator = LoginCoordinator(router: router)
    public lazy var router = AppDelegateRouter(window: window!)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        coordinator.present(animated: true, onDismissed: nil)
        return true
    }

}

extension AppDelegate {
    static var shared: AppDelegate? {
        if Thread.isMainThread {
            return UIApplication.shared.delegate as? AppDelegate
        }

        var appDelegate: AppDelegate?
        DispatchQueue.main.sync {
            appDelegate = UIApplication.shared.delegate as? AppDelegate
        }
        return appDelegate
    }
    
}


