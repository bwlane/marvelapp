//
//  Configuration.swift
//  MarvelApp
//
//  Created by Brian Lane on 5/2/21.
//  Copyright © 2021 Brian Lane. All rights reserved.
//

import UIKit

func isIpad() -> Bool {
    UIDevice.current.userInterfaceIdiom == .pad
}

var publicApiKey: String {
    // Fail loudly if this is not in the property list
    return Bundle.main.object(forInfoDictionaryKey: "PUBLIC_API_KEY") as! String
}

extension String {
    static var timestamp: String {
        let formatter = DateFormatter()
        formatter.timeStyle = .full
        let now = Date()
        return "\(formatter.string(from: now))"
    }
    
    // monks didn't use spaces!
    static var medievalTimeStamp: String {
        timestamp.replacingOccurrences(of: " ", with: "")
    }
}
