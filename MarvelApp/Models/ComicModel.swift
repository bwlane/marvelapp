//
//  ComicModel.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/28/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import UIKit

public struct ComicModel: Decodable {
    // MARK: - Class Variables
    static let isoDateFormatter = ISO8601DateFormatter()
    static let dateFormatter = DateFormatter()
    
    // MARK: - Instance Variables
    var attributionText: String?
    var id: Int?
    var title: String?
    var issueNumber: Int?
    var description: String? = ""
    var releaseDate: String?
    var imageURL: String?
    var imageExtension: String?
    
    // MARK: - Convenience Properties
    var idAsString: String? {
        guard let id = id else { return nil }
        return String(id)
    }
    
    var releaseDatePretty: String? {
        guard let releaseDate = releaseDate else { return nil }
        let date = ComicModel.isoDateFormatter.date(from: releaseDate)!
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        let finalDate = calendar.date(from: components)
        ComicModel.dateFormatter.dateStyle = .medium
        let dateString = ComicModel.dateFormatter.string(from: finalDate ?? Date())
        return dateString
    }
    
    public init() {}
    
    // MARK: - CodingKeys
    
    enum CodingKeys: String, CodingKey {
        case releaseDate = "date"
        case imageURL = "path"
        case attributionText = "attributionText"
        case data = "data"
        enum DataKeys: String, CodingKey {
            case results = "results"
            enum ResultsKeys: String, CodingKey {
                case comics
                enum ComicKeys: String, CodingKey {
                    case id
                    case description
                    case title
                    case issueNumber
                    case dates
                    case images
                    enum DateKeys: String, CodingKey {
                        case type, date
                    }
                    enum ImagesKeys: String, CodingKey {
                        case path
                        case extensionKey = "extension"
                    }
                }
            }
        }
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        attributionText = try container.decode(String.self, forKey: .attributionText)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.DataKeys.self, forKey: .data)
        var resultsContainer = try dataContainer.nestedUnkeyedContainer(forKey: .results)
        while !resultsContainer.isAtEnd {
            let innerResultContainer = try resultsContainer.nestedContainer(keyedBy: CodingKeys.DataKeys.ResultsKeys.ComicKeys.self)
            id = try innerResultContainer.decode(Int.self, forKey: .id)
            title = try innerResultContainer.decode(String.self, forKey: .title)
            issueNumber = try innerResultContainer.decode(Int.self, forKey: .issueNumber)
            description = try innerResultContainer.decodeIfPresent(String.self, forKey: .description)
            var datesContainer = try innerResultContainer.nestedUnkeyedContainer(forKey: .dates)
            while !datesContainer.isAtEnd {
                let dateContainer = try datesContainer.nestedContainer(keyedBy: CodingKeys.DataKeys.ResultsKeys.ComicKeys.DateKeys.self)
                let type = try dateContainer.decode(String.self, forKey: .type)
                if type == "onsaleDate" {
                    self.releaseDate = try dateContainer.decode(String.self, forKey: .date)
                }
            }
            var imagesContainer = try innerResultContainer.nestedUnkeyedContainer(forKey: .images)
            while !imagesContainer.isAtEnd {
                let imageContainer = try imagesContainer.nestedContainer(keyedBy: CodingKeys.DataKeys.ResultsKeys.ComicKeys.ImagesKeys.self)
                self.imageURL = try imageContainer.decodeIfPresent(String.self, forKey: .path)
                self.imageExtension = try imageContainer.decodeIfPresent(String.self, forKey: .extensionKey)
                
            }
        }
    }
}


// MARK: - Convenience Wrapper for JSON Decoding
public struct ComicUnkeyedContainer: Decodable {
    var offset: Int?
    var total: Int?
    var comics: [ComicModel]?
    var available: Int?
    
    // MARK: - CodingKeys
    enum CodingKeys: String, CodingKey {
        case data = "data"
        enum DataKeys: String, CodingKey {
            case offset
            case total
            case results = "results"
            enum ResultsKeys: String, CodingKey {
                case comics
                enum ComicKeys: String, CodingKey {
                    case id
                    case description
                    case title
                    case issueNumber
                    case dates
                    case images
                    enum DateKeys: String, CodingKey {
                        case type, date
                    }
                    enum ImagesKeys: String, CodingKey {
                        case path
                        case extensionKey = "extension"
                    }
                }
            }
        }
    }
    
    public init() {}
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.DataKeys.self, forKey: .data)
        self.offset = try dataContainer.decode(Int.self, forKey: .offset)
        self.total = try dataContainer.decode(Int.self, forKey: .total)
        var resultsContainer = try dataContainer.nestedUnkeyedContainer(forKey: .results)
        var comicModels = [ComicModel]()
        while !resultsContainer.isAtEnd {
            var comic = ComicModel()
            let innerResultContainer = try resultsContainer.nestedContainer(keyedBy: CodingKeys.DataKeys.ResultsKeys.ComicKeys.self)
            comic.id = try innerResultContainer.decode(Int.self, forKey: .id)
            comic.title = try innerResultContainer.decode(String.self, forKey: .title)
            comic.issueNumber = try innerResultContainer.decode(Int.self, forKey: .issueNumber)
            comic.description = try innerResultContainer.decodeIfPresent(String.self, forKey: .description)
            var datesContainer = try innerResultContainer.nestedUnkeyedContainer(forKey: .dates)
            while !datesContainer.isAtEnd {
                let dateContainer = try datesContainer.nestedContainer(keyedBy: CodingKeys.DataKeys.ResultsKeys.ComicKeys.DateKeys.self)
                let type = try dateContainer.decode(String.self, forKey: .type)
                if type == "onsaleDate" {
                    comic.releaseDate = try dateContainer.decode(String.self, forKey: .date)
                }
            }
            var imagesContainer = try innerResultContainer.nestedUnkeyedContainer(forKey: .images)
            while !imagesContainer.isAtEnd {
                let imageContainer = try imagesContainer.nestedContainer(keyedBy: CodingKeys.DataKeys.ResultsKeys.ComicKeys.ImagesKeys.self)
                comic.imageURL = try imageContainer.decode(String.self, forKey: .path)
                comic.imageExtension = try imageContainer.decode(String.self, forKey: .extensionKey)
            }
            comicModels.append(comic)
        }
        self.comics = comicModels
    }
}

