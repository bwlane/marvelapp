//
//  KeyResponse.swift
//  MarvelApp
//
//  Created by Brian Lane on 12/30/19.
//  Copyright © 2019 Brian Lane. All rights reserved.
//

import Foundation

struct KeyResponse: Decodable {
    var key: String?
    enum CodingKeys: String, CodingKey {
        case key = "secret_key"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.key = try container.decode(String.self, forKey: .key)
    }
}
